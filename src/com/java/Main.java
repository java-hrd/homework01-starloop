package com.java;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

    private static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
	// write your code here

        while(true) {

            System.out.println("1. Print Shapes");
            System.out.println("2. Exit");
            System.out.print("\nEnter Option : ");  String option = scan.nextLine();

            switch(option) {
                case "1" :
                    initStarLoop();
                    break;
                case "2" :
                    System.exit(0);
                    break;
                default:
                    System.out.println("\nPLEASE INPUT VALID NUMBER\n\n");
            } // end of switch

        } // end of while

    } // end of main



    //    INIT STAR LOOP
    public static void initStarLoop() {
        System.out.print("\nInput Number of Row  :  ");      String rows = scan.nextLine();

        boolean isNumber = Pattern.matches("[0-9]+", rows);    // Check if number or not

        if(isNumber) {
            int numRows = Integer.parseInt(rows);

            if( numRows < 7) {
                System.out.println("\nNUMBER OF ROW MUST BE MORE THAN 6\n\n");
            }else if(rows.length() > 3) {
                System.out.println("\nNUMBER OF ROW MUST BE BETWEEN [7, 999]\n\n");
            }else {
                Main star = new Main();
                System.out.println("1.  Shape I\n");
                star.shape01(numRows);

                System.out.println("\n\n2.  Shape II\n");
                star.shape02(numRows);

                System.out.println("\n\n3.  Shape III\n");
                star.shape03(numRows);

                System.out.println("\n\n4.  Shape V\n");
                star.shape04(numRows);

                System.out.println("\n\n5.  Shape V\n");
                star.shape05(numRows);

                System.out.println("\n\n6.  Shape VI\n");
                star.shape06(numRows);

                System.out.println("\n\n7. Shape VII\n");
                star.shape07(numRows);

                System.out.println("\n\n8. Shape VIII\n");
                star.shape08(numRows);

                System.out.println("\n\n9. Shape IX\n");
                star.shape09(numRows);

                System.out.println("\n\n10. Shape X\n");
                star.shape10(numRows);

                System.out.println("\n\n11. Shape XI\n");
                star.shape11(numRows);
            }

        }else {
            if(rows.startsWith("-"))
                System.out.println("\nCANNOT INPUT NEGATIVE NUMBER\n\n");
            else if(Pattern.matches("^[a-zA-Z0-9]*$", rows))
                System.out.println("\nCANNOT INPUT ANY CHARACTERS\n\n");
            else if(Pattern.matches(".*[$&+,:;=\\?@#|/' <>.^*()%!-].*", rows))
                System.out.println("\nCANNOT INPUT ANY SYMBOLS\n\n");
            else
                System.out.println("\nINVALID NUMBER !\n\n");
        }

    }

    // SHAPE 1
    public void shape01(int numRows) {

        for(int i=0; i<numRows+1; i++) {
            for(int j=0; j<numRows-i; j++)
                System.out.print(" ");
            for(int k=1; k<i+1; k++)
                System.out.print("*");
            for(int k=i-1; k>=1; k--)
                System.out.print("*");
            System.out.println();
        }
    }

    // SHAPE 2
    public void shape02(int numRows) {

        for(int i=0; i<numRows; i++) {
            for(int j=0; j<i+1; j++)
                System.out.print(" ");

            for(int j=0; j<numRows-i; j++)
                if(j == numRows-i-1)
                    System.out.print("*");
                else
                    System.out.print("**");

            System.out.println();
        }
    }

    // SHAPE 3
    public void shape03(int numRows) {

        shape01(numRows/2+1);
        shape02(numRows/2);

    }

    // SHAPE 4
    public void shape04(int numRows) {

        for(int i=0; i<numRows; i++) {
            for(char j='A'; j<i+'A' +1; j++)
                System.out.print(j);
            System.out.println();
        }
    }

    // SHAPE 5
    public void shape05(int numRows) {

        for(int i=0; i<numRows; i++) {
            for(char j='A'; j<numRows-i+'A'; j++)
                System.out.print(j);
            System.out.println();
        }
    }

    // SHAPE 6
    public void shape06(int numRows) {

        // TOP
        for(int i=0; i<numRows/2; i++) {
            for(int j=0; j<numRows/2-i; j++) {
                if (j == numRows / 2 - i - 1)
                    System.out.print("*");
                else
                    System.out.print("* ");
            }
            for(int j=0; j<2*i;j++) {
                System.out.print("  ");
            }
            for(int j=0; j<numRows/2-i; j++) {
                System.out.print(" *");
            }
            System.out.println();
        }

        // BOTTOM
        for(int i=0; i<numRows/2; i++) {
            for(int j=0; j<i+1; j++) {
                if (j == i)
                    System.out.print("*");
                else
                    System.out.print("* ");
            }
            for(int j=i; j<numRows-i - (numRows%2+2); j++) {
                System.out.print("  ");
            }
            for(int j=0; j<i+1; j++) {
                System.out.print(" *");
            }
            System.out.println();
        }
    }


    // SHAPE 7
    public void shape07(int numRows) {
        int rows = (numRows % 2 == 0) ? (numRows /2) : (numRows / 2 + 1);

        // TOP
        for (int i=1; i<=rows; i++) {
            for (int j = 0; j < i; j++) {
                String star = (j == i-1) ? "*" : "* ";
                System.out.print(star);
            }
            for (int k = i*2; k <= rows*2-1; k++) {
                System.out.print("  ");
            }
            for (int l = 1; l <= i; l++) {
                System.out.print(" *");
            }
            System.out.println();
        }

        // BOTTOM
        for (int i=1; i<=rows-1; i++) {
            for (int j = rows-1; j >= i; j--) {
                String star = (j == i) ? "*" : "* ";
                System.out.print(star);
            }
            for (int k = 1; k <= i*2; k++) {
                System.out.print("  ");
            }
            for (int l = rows-1; l >= i; l--) {
                System.out.print(" *");
            }

            System.out.println();
        }
    }

    // SHAPE 08
    public void shape08(int numRows) {
        int rows = (numRows % 2 == 0) ? (numRows /2) : (numRows / 2 + 1);
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            for (int j = i - 1 + 'A'; j <= rows - 1 + 'A'; j++) {
                System.out.print((char)j+" ");
            }
            System.out.println();
        }

        for (int i = rows-1; i >= 1; i--) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            for (int j = i -1 + 'A'; j <= rows - 1 + 'A'; j++) {
                System.out.print((char)j+" ");
            }

            System.out.println();
        }
    }

    // SHAPE 09
    public void shape09(int numRows) {
        for(int i = 1; i <= numRows; i++) {
            for (int j = numRows-1; j>=i; j--) {
                System.out.print(" ");
            }
            // Print star in decreasing order
            for(int k='A' + i-1; k<=numRows-1-1 + i + 'A'; k++) {
                System.out.print((char)k + " ");
            }
            System.out.println();
        }
    }

    // SHAPE 10
    public void shape10(int numRows) {
        for(int i=1; i<= numRows; i++) {
            if(i%2 != 0) {
                for(int j=1; j<= numRows/2+1; j++) {
                    System.out.print("*  ");
                }
            } else {
                for(int j=1; j<= numRows/2; j++) {
                    System.out.print(" * ");
                }
            }
            System.out.println("");
        }
    }

    // SHAPE 11
    public void shape11(int numRows) {
        int rows = (numRows % 2 == 0) ? (numRows /2) : (numRows / 2 + 1);

        for (int i=1; i<=(rows * 2 -1); i++) {
            if( i == rows) {
                // Printing Horizontal Line of Stars
                for (int j=1; j<=(rows * 2 -1); j++) {
                    System.out.print("*");
                }
            }
            else {
                // Printing space before Vertical Line of Stars
                for(int k=1; k<= rows-1; k++) {
                    System.out.print(" ");
                }
                System.out.print("*");
            }
            System.out.println();
        }
    }

}
